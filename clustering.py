# -*- coding= utf8 -*-

# from functions import d2, N, G, flicm
import numpy as np
from matplotlib import pyplot as plt
from numpy import random

from FLICM import Flicm


# Define Data
N = 100
data1 = random.rand(N, 2)
# data[:, 1] *= 10
# values = np.ones(N)
values = random.rand(N) * 0.2

data2 = random.rand(N, 2) + 0.6
# values2 = np.zeros(N)
values2 = random.rand(N) * 0.8 + 1

data = np.concatenate((data1, data2), axis=0)
values = np.concatenate((values, values2), axis=0)


# define the instance
flicm = Flicm(data, values)

# change initial values
flicm.epsilon = 0.5

# the algorithm
flicm.compute_clusters()

# The plots
flicm.plot_clusters()
flicm.plot_data()
flicm.plot_errors()

# print centers, clusters and errors
print flicm.centers
print flicm.clusters
print flicm.errors


# test some data
x = np.linspace(0, 2, 20)
tests = np.asarray([[x0, y0] for x0 in x for y0 in x])

flicm.defuzzify_data(tests)
flicm.plot_defuzzy_data()

