# -*- coding= utf8 -*-

# from functions import d2, N, G, flicm
import numpy as np
from matplotlib import pyplot as plt
from numpy import random
# Euclidean distance
from scipy.spatial.distance import euclidean as d
from itertools import cycle


# squared Euclidean distance
def d2(x, y):
    result = d(x, y)**2
    return result


# # Neighborhood
# Given a list (or a numpy array) and an index of the list (integer)
# it returns the list sorted according to the distance from the indexed element
# Closest elements go first
# data: a list of points in Rn
# index: the index of the point which its neighborhood will be found
def N(data, element):
    # data = np.asarray(data)
    # distances = list()
    distances = np.empty(0)
    for i in range(len(data)):
        distance = d(data[i], element)
        # distances.append(distance)
        distances = np.concatenate([distances, [distance]])
    # distances = np.asarray(distances)
    distances_index = np.argsort(distances)
    result = data[distances_index]
    return result


# data: the data array
# k: index of the cluster (integer)
# centers: the list of centers of the clusters
# i: an element of the data array
# U: membership matrix
# neighborhood_N: length of neighborhood (integer)
def G(data, k, i, centers, values, U, neighborhood_N=10, m=2):

    # find index of i to data
    # for j in range(len(data)):
    #     if (data[j] == i).all():
    #         index = j
    #         break

    products = list()
    # iterate through all neighbors
    l = 0
    for j in N(data, i)[1:neighborhood_N]:
        # compute factor 1
        factor_1 = 1/(d(i, j)+1)

        # compute factor 2
        for l in range(len(data)):
            if (data[l] == j).all():
                index = l
                break
        factor_2 = (1 - U[index][k])**m  # the u_kj

        # compute factor 3
        # factor_3 = d2(j, centers[k])
        factor_3 = d2(values[l], centers[k])

        # multiply factors
        product = factor_1 * factor_2 * factor_3
        # append product to a list of all products
        products.append(product)
        l += 1
    # return te sum of the products
    return sum(products)


class Flicm:

    def __init__(self, data, values):
        self.data = data
        self.values = values

        self.neighborhood_N = 10
        self.m = 2
        self.epsilon = 0.01

        self.c = 2

        self.U = None
        self.clusters = None
        self.centers = None
        self.errors = list()

        self.defuzzy_list = list()
        self.tests = None

    def compute_clusters(self):

        data = self.data
        values = self.values
        m = self.m
        epsilon = self.epsilon

        # STEP 1
        c = self.c
        # define m
        # define epsilon

        # STEP 2
        # this is U
        # initialize randomly membership matrix
        U = list()
        for i in range(len(data)):
            value = random.random()
            U.append([value, 1-value])

        # print data
        # print U

        # STEP 3
        b = 0

        # STEP 4
        # calculate the cluster prototypes
        # these are the center of each cluster
        while True:
            centers = list()
            for k in range(c):
                numerator_elements = list()
                for i in range(len(data)):
                    numerator_element = (U[i][k] ** m) * data[i]
                    numerator_elements.append(numerator_element)
                numerator_elements = np.asarray(numerator_elements)
                numerator = sum(numerator_elements)
                # print numerator, '----'

                denominator_elements = list()
                for i in range(len(data)):
                    denominator_elements.append(U[i][k] ** m)
                denominator = sum(denominator_elements)
                # print denominator, '===='

                centers.append(1 / denominator * numerator)

            centers = np.asarray(centers)

            # STEP 5
            # compute membership values
            U_old = U
            U = [[] for i in range(len(data))]
            for i in range(len(data)):
                for k in range(c):
                    G_1 = G(data, k, data[i], centers, values, U_old, 4)
                    elements = list()
                    for j in range(c):
                        G_2 = G(data, j, data[i], centers, values, U_old, 4)
                        element = ((d2(data[i], centers[k]) + G_1) / (d2(data[i], centers[j]) + G_2)) ** (1 / (m - 1))
                        elements.append(element)
                    U[i].append(1 / sum(elements))
            # print U_old
            # print U

            differences = list()
            for i in range(len(U)):
                dif = abs(U[i][0] - U_old[i][0])
                differences.append(dif)
            # print differences
            # print max(differences)

            if max(differences) < epsilon:
                # print 'max difference is ', max(differences)
                # print 20 * '#' + ' terminated after {} iterations '.format(b) + 20 * '#'
                # print 50 * '-'
                # print 'matrix U'
                # print 50 * '-'
                # print U

                # Defuzify
                U = np.asarray(U)
                clusters = [[] for i in range(c)]
                for i in range(len(U)):
                    a = U[i].argmax()
                    clusters[a].append(data[i])
                # clusters = np.asarray(clusters)
                # return U, clusters, centers
                for i in range(len(clusters)):
                    clusters[i] = np.asarray(clusters[i])

                self.U = U
                self.clusters = clusters
                self.centers = centers
                self.errors.append(max(differences))
                break
            else:
                m_dif = max(differences)
                print 'loop:', b, ' ---> epsilon:', m_dif
                self.errors.append(m_dif)
                b += 1

    def plot_clusters(self):
        sh_co = ('bo', 'ro')
        cycle_sh_co = cycle(sh_co)

        for i in self.clusters:
            plt.plot(i.T[0], i.T[1], next(cycle_sh_co), markersize=10)
        centers = self.centers
        plt.plot(centers.T[0], centers.T[1], 'y.', markersize=50, alpha=0.4)
        plt.show()

    def plot_data(self):
        data = self.data
        values = self.values
        for i in range(len(data)):
            plt.plot([data[i][0]], data[i][1], 'bo', markersize=10, alpha=values[i])
        plt.show()

    def plot_errors(self):
        errors = self.errors
        plt.plot(range(len(errors)), errors)
        plt.plot(range(len(errors)), errors, 'ro')
        plt.show()

    def defuzzify_data(self, tests):
        print 'this may take some time'
        self.tests = tests
        self.defuzzy_list = list()
        for test in tests:
            U_test = list()
            for k in range(self.c):
                G_1 = G(self.data, k, test, self.centers, self.values, self.U, 4)
                elements = list()
                for j in range(self.c):
                    G_2 = G(self.data, j, test, self.centers, self.values, self.U, 4)
                    element = ((d2(test, self.centers[k]) + G_1) / (d2(test, self.centers[j]) + G_2)) ** (1 / (self.m - 1))
                    elements.append(element)
                U_test.append(1 / sum(elements))
            # print U_test
            U_test = np.asarray(U_test)
            a = np.argmax(U_test)
            self.defuzzy_list.append(a)


    def plot_defuzzy_data(self):
        tests = self.tests
        if len(self.defuzzy_list) > 0:
            for a in range(len(self.defuzzy_list)):
                if self.defuzzy_list[a] == 0:
                    plt.plot([tests[a][0]], [tests[a][1]], 'b^', markersize=10)
                else:
                    plt.plot([tests[a][0]], [tests[a][1]], 'r^', markersize=10)
            plt.show()
        else:
            print 'run first --> instance.defuzzify_data(data)'


if __name__ == '__main__':

    # the data
    data = np.asarray([
        [1, 1],
        [1, 2],
        [2, 2],
        [4, 3],
        [7, 10],
        [7, 9],
        [8, 11],
        [8, 10],
        [2, 1],
        [4, 1]
    ])
    values = np.asarray([0, 1, 0, 1, 0, 1, 0, 1, 0, 1])

    flicm = Flicm(data, values)
    flicm.compute_clusters()
    for i in flicm.clusters:
        print 30*'-', '> cluster'
        print i
        print 40*'-'
    print 30*'-', '> centers'
    print flicm.centers
