# -*- coding= utf8 -*-

from __future__ import division

# Euclidean distance
from scipy.spatial.distance import euclidean as d

import numpy as np
from matplotlib import pyplot as plt
import random

# squared Euclidean distance
def d2(x, y):
    result = d(x, y)**2
    return result


# # Neighborhood
# Given a list (or a numpy array) and an index of the list (integer)
# it returns the list sorted according to the distance from the indexed element
# Closest elements go first
# data: a list of points in Rn
# index: the index of the point which its neighborhood will be found

# def N(data, index):
#     distances = list()
#     for i in range(len(data)):
#         distance = d(data[i], data[index])
#         distances.append(distance)
#     distances = np.asarray(distances)
#     distances_index = np.argsort(distances)
#     return data[distances_index]
def N(data, element):
    # data = np.asarray(data)
    # distances = list()
    distances = np.empty(0)
    for i in range(len(data)):
        distance = d(data[i], element)
        # distances.append(distance)
        distances = np.concatenate([distances, [distance]])
    # distances = np.asarray(distances)
    distances_index = np.argsort(distances)
    result = data[distances_index]
    return result


# data: the data array
# k: index of the cluster (integer)
# centers: the list of centers of the clusters
# i: an element of the data array
# U: membership matrix
# neighborhood_N: length of neighborhood (integer)
def G(data, k, i, centers, values, U, neighborhood_N=10, m=2):

    # find index of i to data
    # for j in range(len(data)):
    #     if (data[j] == i).all():
    #         index = j
    #         break

    products = list()
    # iterate through all neighbors
    l = 0
    for j in N(data, i)[1:neighborhood_N]:
        # compute factor 1
        factor_1 = 1/(d(i, j)+1)

        # compute factor 2
        for l in range(len(data)):
            if (data[l] == j).all():
                index = l
                break
        factor_2 = (1 - U[index][k])**m  # the u_kj

        # compute factor 3
        # factor_3 = d2(j, centers[k])
        factor_3 = d2(values[l], centers[k])

        # multiply factors
        product = factor_1 * factor_2 * factor_3
        # append product to a list of all products
        products.append(product)
        l += 1
    # return te sum of the products
    return sum(products)


# returns ( matrix U, clusters, centers )
def flicm(data, values, m=2, epsilon=0.01):

    # STEP 1
    c = len(data[0])
    # define m
    # define epsilon

    # STEP 2
    # this is U
    # initialize randomly membership matrix
    U = list()
    for i in range(len(data)):
        value = random.random()
        U.append([value, 1-value])

    # print data
    print U

    # STEP 3
    b = 0

    # STEP 4
    # calculate the cluster prototypes
    # these are the center of each cluster
    while True:
        centers = list()
        for k in range(c):
            numerator_elements = list()
            for i in range(len(data)):
                numerator_element = (U[i][k] ** m) * data[i]
                numerator_elements.append(numerator_element)
            numerator_elements = np.asarray(numerator_elements)
            numerator = sum(numerator_elements)
            # print numerator, '----'

            denominator_elements = list()
            for i in range(len(data)):
                denominator_elements.append(U[i][k] ** m)
            denominator = sum(denominator_elements)
            # print denominator, '===='

            centers.append(1 / denominator * numerator)

        centers = np.asarray(centers)

        # STEP 5
        # compute membership values
        U_old = U
        U = [[] for i in range(len(data))]
        for i in range(len(data)):
            for k in range(c):
                G_1 = G(data, k, data[i], centers, values, U_old, 4)
                elements = list()
                for j in range(c):
                    G_2 = G(data, j, data[i], centers, values, U_old, 4)
                    element = ((d2(data[i], centers[k]) + G_1) / (d2(data[i], centers[j]) + G_2)) ** (1 / (m - 1))
                    elements.append(element)
                U[i].append(1 / sum(elements))
        # print U_old
        # print U

        differences = list()
        for i in range(len(U)):
            dif = abs(U[i][0] - U_old[i][0])
            differences.append(dif)
        # print differences
        # print max(differences)

        if max(differences) < epsilon:
            print 'max difference is ', max(differences)
            print 20 * '#' + ' terminated after {} iterations '.format(b) + 20 * '#'
            print 50 * '-'
            print 'matrix U'
            print 50 * '-'
            print U

            # Defuzify
            U = np.asarray(U)
            clusters = [[] for i in range(c)]
            for i in range(len(U)):
                a = U[i].argmax()
                clusters[a].append(data[i])

            return U, clusters, centers

            # break
        else:
            print 'loop:', b, ' ---> epsilon:', max(differences)
            b += 1


if __name__ == '__main__':

    # # test function N
    # A = np.asarray([1, 2, 3, 4, 1, 2, 3, 4])
    # print A
    # print N(A, 1)


    # the data
    data = np.asarray([
        [1, 1],
        [1, 2],
        [2, 2],
        [4, 3],
        [7, 10],
        [7, 9],
        [8, 11],
        [8, 10],
        [2, 1],
        [4, 1]
    ])
    values = np.asarray([0, 1, 0, 1, 0, 1, 0, 1, 0, 1])

    flicm_data = flicm(data, values)

    # test in which cluster [6,6] belongs
    c = 2
    m = 2
    U = flicm_data[0]
    centers = flicm_data[2]
    test = np.asarray([6, 6])
    U_test = list()
    for k in range(c):
        G_1 = G(data, k, test, centers, values, U, 4)
        elements = list()
        for j in range(c):
            G_2 = G(data, j, test, centers, values, U, 4)
            element = ((d2(test, centers[k]) + G_1) / (d2(test, centers[j]) + G_2))**(1/(m-1))
            elements.append(element)
        U_test.append(1/sum(elements))
    print U_test
    U_test = np.asarray(U_test)
    a = np.argmax(U_test)

    clusters = flicm_data[1]

    c_0 = np.asarray(clusters[0])
    c_1 = np.asarray(clusters[1])

    plt.plot(c_0.T[0], c_0.T[1], 'b+')
    plt.plot(c_1.T[0], c_1.T[1], 'r+')

    if a == 0:
        plt.plot([test[0]], [test[1]], 'bo')
    else:
        plt.plot([test[0]], [test[1]], 'ro')

    plt.show()










